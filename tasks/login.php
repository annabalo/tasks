<?php

session_start();
if (isset($_POST["login"]) && !isset($_SESSION["login"])) {
  $users = [
    "a" => "xyz",
    "b" => "zzz"
  ];

  if (isset($users[$_POST["login"]])) {
    if ($users[$_POST["login"]] == $_POST["password"]) {
      $_SESSION["login"] = $_POST["login"];
    }
  }

  if (!isset($_SESSION["login"])) {
    $fail = TRUE;
  }
}

if (isset($_SESSION["login"])) {
  header("location:index.php");
  exit();
}