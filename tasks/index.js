function solve(st,a,b) {
    if (a < 0 || b > st.length - 1) {
        console.log('index out of bounds');
        return;
    }
    let substr1 = st.substring(0,a)
    let substrToReverse = st.substring(a,b+1);
    let substr2 = st.substring(b+1, st.length);
    let reversedSubstr = substrToReverse.split("").reverse().join("");
    let result = substr1.concat(reversedSubstr, substr2)
    console.log(result)
    return result;
}