<?php
session_start();

if (isset($_POST["logout"])) {
  unset($_SESSION["login"]);
}

if (!isset($_SESSION["login"])) {
  header("location:login.php");
  exit();
}