<?php

class Path
{
  public $currentPath;

  function __construct($path)
  {
    $this->currentPath = $path;
  }

  public function cd($newPath)
  {
    $dirsOld = explode('/', $this->currentPath);
    $dirsNew = explode('/', $newPath);
    foreach ($dirsNew as $dir) {
      if ($dir == '..') {
        array_pop($dirsOld);
      }
      else {
        array_push($dirsOld, $dir);
      }
    }
    $this->currentPath = implode('/', $dirsOld);
    return $this->currentPath;
  }
}

$path = new Path('/a/b/c/d');
$path->cd('../x');
echo $path->currentPath;
